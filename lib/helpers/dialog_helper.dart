// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_challenge_test/constant/color_constant.dart';
import 'package:flutter_challenge_test/constant/text_constant.dart';
import 'package:get/get.dart';

class DialogHelper {
  static showFailed() {
    showDialog(
        icon: "assets/remove.png",
        label: "Login Failed",
        desc: "User ID dan atau Password anda belum diisi");
  }

  static showSuccess() {
    showDialog(
        icon: "assets/check.png",
        label: "Login Success",
        desc: "Login Berhasil");
  }

  static showDialog({
    required icon,
    required label,
    required desc,
  }) {
    Get.dialog(
      Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 24, horizontal: 12),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                icon,
                height: 48,
                width: 48,
              ),
              SizedBox(height: 12),
              Text(
                label,
                style: AppTextStyles.text18Bold,
              ),
              Text(
                desc,
                textAlign: TextAlign.center,
                style: AppTextStyles.text14.copyWith(
                  color: AppColors.darkGreyColor,
                ),
              ),
              SizedBox(height: 12),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: AppColors.primaryColor,
                    padding: EdgeInsets.symmetric(horizontal: 36),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50))),
                onPressed: () {
                  Get.back();
                },
                child: Text("Ok"),
              ),
            ],
          ),
        ),
      ),
      barrierDismissible: false,
    );
  }
}
