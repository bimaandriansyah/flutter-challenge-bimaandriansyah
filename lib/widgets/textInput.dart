// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter_challenge_test/constant/color_constant.dart';
import 'package:flutter_challenge_test/constant/text_constant.dart';

Widget textInput({
  required label,
  var controller,
  required TextInputType inputType,
  required TextInputAction inputAction,
  required validation,
}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        label,
        style: AppTextStyles.text14,
      ),
      TextFormField(
        keyboardType: inputType,
        textInputAction: inputAction,
        controller: controller,
        style: AppTextStyles.text14,
        decoration: InputDecoration(
          hintText: label,
          hintStyle:
              AppTextStyles.text14.copyWith(color: AppColors.lightGreyColor),
        ),
        validator: validation,
      )
    ],
  );
}
