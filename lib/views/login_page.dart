// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:flutter_challenge_test/constant/color_constant.dart';
import 'package:flutter_challenge_test/constant/text_constant.dart';
import 'package:flutter_challenge_test/controller/login_controller.dart';
import 'package:flutter_challenge_test/widgets/textInput.dart';
import 'package:flutter_challenge_test/widgets/textInputPassword.dart';
import 'package:get/get.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final LoginController loginC = Get.put(LoginController());
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                alignment: Alignment.centerLeft,
                children: [
                  Image.asset(
                    "assets/header-login.png",
                    width: Get.width / 3,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Image.asset(
                      "assets/logo.png",
                      width: Get.width / 3.6,
                    ),
                  )
                ],
              ),
              SizedBox(height: 48),
              Container(
                margin: EdgeInsets.all(24),
                child: Form(
                  key: loginC.formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Login",
                        style: AppTextStyles.text24Bold,
                      ),
                      Text(
                        "Please sign in to continue.",
                        style: AppTextStyles.text14,
                      ),
                      SizedBox(height: 24),
                      textInput(
                          label: "User ID",
                          controller: loginC.userIDFC,
                          inputAction: TextInputAction.next,
                          inputType: TextInputType.text,
                          validation: (value) {
                            if (value == null || value.isEmpty) {
                              return 'User ID masih kosong';
                            }
                            return null;
                          }),
                      SizedBox(height: 24),
                      Obx(
                        () => textInputPassword(
                            label: "Password",
                            controller: loginC.passwordFC,
                            inputType: TextInputType.visiblePassword,
                            inputAction: TextInputAction.done,
                            isShow: loginC.onShow.value,
                            clickShow: () {
                              loginC.showPassword();
                            },
                            validation: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Password masih kosong';
                              }
                              return null;
                            }),
                      ),
                      SizedBox(height: 24),
                      Align(
                        alignment: Alignment.centerRight,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: AppColors.primaryColor,
                              padding: EdgeInsets.symmetric(horizontal: 36),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50))),
                          onPressed: () {
                            loginC.onSend();
                          },
                          child: Text("Login"),
                        ),
                      ),
                      SizedBox(height: 204),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Don't have an account? ",
                            style: AppTextStyles.text12
                                .copyWith(color: AppColors.darkGreyColor),
                          ),
                          Text(
                            "Sign Up",
                            style: AppTextStyles.text12Bold
                                .copyWith(color: AppColors.dangerColor),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
