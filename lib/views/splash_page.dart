// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_challenge_test/routes/pages.dart';
import 'package:get/get.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    splashscreenStart();
  }

  splashscreenStart() async {
    var duration = const Duration(seconds: 2);
    return Timer(duration, () {
      Get.offAllNamed(AppPages.LOGIN);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height,
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
              child: Image.asset(
            "assets/header-splash.png",
            fit: BoxFit.cover,
          )),
          Expanded(child: Image.asset("assets/logo.png")),
          Expanded(
              child: Image.asset(
            "assets/footer-splash.png",
            fit: BoxFit.cover,
          )),
        ],
      ),
    );
  }
}
