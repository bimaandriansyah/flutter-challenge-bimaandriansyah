import 'package:flutter/material.dart';
import 'package:flutter_challenge_test/constant/color_constant.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTextStyles {
  static TextStyle titleHeader = GoogleFonts.poppins(
    fontSize: 28,
    fontWeight: FontWeight.w700,
    color: AppColors.blackColor,
  );

  static TextStyle text16 = GoogleFonts.poppins(
    fontSize: 16,
    color: AppColors.blackColor,
  );

  static TextStyle text18Bold = GoogleFonts.poppins(
    fontSize: 18,
    fontWeight: FontWeight.w600,
    color: AppColors.blackColor,
  );

  static TextStyle text16Bold = GoogleFonts.poppins(
    fontSize: 16,
    fontWeight: FontWeight.w600,
    color: AppColors.blackColor,
  );

  static TextStyle text12 = GoogleFonts.poppins(
    fontSize: 12,
    color: AppColors.blackColor,
  );

  static TextStyle text12Bold = GoogleFonts.poppins(
    fontSize: 12,
    color: AppColors.blackColor,
    fontWeight: FontWeight.w600,
  );

  static TextStyle text10 = GoogleFonts.poppins(
    fontSize: 10,
    color: AppColors.blackColor,
  );

  static TextStyle text14Bold = GoogleFonts.poppins(
    fontSize: 14,
    fontWeight: FontWeight.w600,
    color: AppColors.blackColor,
  );

  static TextStyle text14 = GoogleFonts.poppins(
    fontSize: 14,
    color: AppColors.blackColor,
  );

  static TextStyle text20Bold = GoogleFonts.poppins(
    fontSize: 20,
    fontWeight: FontWeight.w600,
    color: AppColors.blackColor,
  );

  static TextStyle text24Bold = GoogleFonts.poppins(
    fontSize: 24,
    fontWeight: FontWeight.w600,
    color: AppColors.blackColor,
  );
}
