// ignore_for_file: unnecessary_overrides, unnecessary_this, argument_type_not_assignable_to_error_handler, file_names

import 'package:flutter/material.dart';
import 'package:flutter_challenge_test/helpers/dialog_helper.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  final formKey = GlobalKey<FormState>();
  final userIDFC = TextEditingController();
  final passwordFC = TextEditingController();
  var onShow = true.obs;

  void showPassword() {
    onShow.value = !onShow.value;
  }

  onSend() async {
    Get.focusScope?.unfocus();
    if (this.formKey.currentState!.validate()) {
      DialogHelper.showSuccess();
    } else {
      DialogHelper.showFailed();
    }
  }
}
