// ignore_for_file: constant_identifier_names

import 'package:flutter_challenge_test/views/login_page.dart';
import 'package:flutter_challenge_test/views/splash_page.dart';
import 'package:get/get.dart';

part 'routes.dart';

class AppPages {
  static const LOGIN = _Paths.LOGIN;
  static const SPLASH = _Paths.SPLASH;
}

abstract class _Paths {
  static const SPLASH = '/splash';
  static const LOGIN = '/login';
}
