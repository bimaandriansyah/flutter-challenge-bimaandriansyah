// ignore_for_file: constant_identifier_names, prefer_const_constructors

part of 'pages.dart';

class AppRoutes {
  static const INITIAL = AppPages.SPLASH;

  static final pages = [
    GetPage(name: _Paths.SPLASH, page: () => SplashPage()),
    GetPage(name: _Paths.LOGIN, page: () => LoginPage()),
  ];
}
